import React, { Component } from 'react';
import {
    AppRegistry,
    Text,
    StyleSheet,
    View,
    StatusBar
} from 'react-native';

export default class Cities extends Component<{}> {
    render() {
        return (
            <View>
                <StatusBar
                    backgoundColor="coral"
                    barStyle="light-content"
                />
                <View style={styles.navbar}>
                    <Text style={styles.navbarTitle}>
                        {this.props.title}
                    </Text>
                </View>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container:{
        backgroundColor: '#f2f2f2',
        flex: 1,
    },
    listview:{
        flex:1,
    },
    li:{
        backgroundColor:'#fff',
        borderBottomColor:'#eee',
        borderColor:'transparent',
        borderWidth:1,
        paddingLeft:16,
        paddingTop:14,
        paddingBottom:16,
    },
    liContainer:{
        flex:1,
    },
    liText:{
        color: '#333',
        fontSize:16,
    },
    navbar: {
        alignItems: 'center',
        backgroundColor: '#fff',
        borderBottomColor: '#eee',
        borderColor: 'transparent',
        borderWidth: 1,
        justifyContent: 'center',
        height: 44,
        flexDirection: 'row'
    },
    navbarTitle: {
        color: '#444',
        fontSize: 16,
        fontWeight: '500'
    },
    toolbar: {
        backgroundColor: '#fff',
        height: 22,
    },
})

AppRegistry.registerComponent('Cities', () => Cities);