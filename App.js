/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from 'react';
import {
  View,
  Text,
  StyleSheet,
  ListView,
  TouchableHighlight
} from 'react-native';

import Cities from './call/components/Cities/Cities'

import * as firebase from 'firebase';

var config = {
    apiKey: "AIzaSyDYzXzWrbHI7r6y0_9wIHCHuzB6KZDDNJw",
    authDomain: "calltaxi-e8e61.firebaseapp.com",
    databaseURL: "https://calltaxi-e8e61.firebaseio.com",
    projectId: "calltaxi-e8e61",
    storageBucket: "calltaxi-e8e61.appspot.com",
    messagingSenderId: "449177389976"
};

const firebaseApp = firebase.initializeApp(config);

export default class App extends Component<{}> {

    constructor(){
        super();
        let ds = new ListView.DataSource({rowHasChanged: (r1, r2) => r1 !== r2});
        this.state = {
            itemDataSource: ds
        };

        this.citiesRef = firebaseApp.database().ref().child("cities/");

        this.renderRow = this.renderRow.bind(this);
        this.pressRow = this.pressRow.bind(this);
    }

    componentWillMount(){
        this.getItems(this.citiesRef);
    }

    componentDidMount(){
        this.getItems(this.citiesRef);
    }

    getItems(citiesRef) {
        citiesRef.on('value', (snap) => {
            let items = [];
            snap.forEach((child) => {
                console.log(child.val().title);
                items.push({
                    title: child.val().title,
                    _key: child.key
                });
            });
            this.setState({
                itemDataSource: this.state.itemDataSource.cloneWithRows(items)
            });
        });
    }

    pressRow(item){
        console.log(item);
    }

    renderRow(item){
        return (
            <TouchableHighlight onPress={() => {
                this.pressRow(item);
            }}>
                <View style={styles.li}>
                    <Text style={styles.liText}>
                        {item.title}
                    </Text>
                </View>
            </TouchableHighlight>
        );
    }

    render() {
    return (
      <View style={styles.container}>
        <Cities title="Cities"/>
        <ListView
          dataSource={this.state.itemDataSource}
          renderRow={this.renderRow}
        />
      </View>
    );
  }
}

const styles = StyleSheet.create({
    container:{
      backgroundColor: '#f2f2f2',
      flex: 1,
    },
    listview:{
        flex:1,
    },
    li:{
      backgroundColor:'#fff',
      borderBottomColor:'#eee',
      borderColor:'transparent',
      borderWidth:1,
      paddingLeft:16,
      paddingTop:14,
      paddingBottom:16,
    },
    liContainer:{
        flex:1,
    },
    liText:{
       color: '#333',
       fontSize:16,
    },
    navbar: {
        alignItems: 'center',
        backgroundColor: '#fff',
        borderBottomColor: '#eee',
        borderColor: 'transparent',
        borderWidth: 1,
        justifyContent: 'center',
        height: 44,
        flexDirection: 'row'
    },
    navbarTitle: {
        color: '#444',
        fontSize: 16,
        fontWeight: '500'
    },
    toolbar: {
        backgroundColor: '#fff',
        height: 22,
    },
});
